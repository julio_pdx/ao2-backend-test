<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
		@include('_partials.head')
    </head>
    <body>

		<main class="container">

			@include('persons._header')

			@if ($message = Session::get('success'))
				<div class="alert alert-success" role="alert">
					<p>{{ $message }}</p>
				</div>
			@endif

			<!-- Person List-->
			<div class="my-3 p-3 bg-body rounded shadow-sm">
				<h6 class="border-bottom pb-2 mb-0">Cadastros</h6>

				<!--
					Lista de pessoas cadastradas
				-->

				@if(empty($data))
					<p>Nenhum cadastro encontrado em nossa base de dados. <a href="{{ route('persons.create') }}">Clique aqui</a> e comece o cadastro agora mesmo!</p>
				@else

					@foreach ( $data as $key => $user )
					<div class="lh-sm border-bottom">
						<div class="d-flex text-muted pt-3">
							<img class="flex-shrink-0 me-2 rounded" src="//via.placeholder.com/32x32.png?text=AO" width="32" height="32" />

							<p class="pb-3 mb-0 small">
								<a href="{{ route('persons.edit', $user->id) }}"><strong class="d-block text-gray-dark">{{ '@' . $user->username }} &lt;{{ $user->email }}&gt;</strong></a>
								{{ $user->description }}
							</p>

						</div>
						<div class="text-muted pt-1">

							<form action="{{ route('persons.destroy',$user->id) }}" method="POST" class="small text-end">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input name="_method" type="hidden" value="DELETE">
								{{ $user->created_at->format('d/m/Y') }} |
								<button class="btn btn-sm btn-danger" type="submit"><i class="bi bi-trash small"></i></button>
							</form>

						</div>
					</div>
					@endforeach
				@endif

				<div class="d-flex justify-content-between mt-3">
					<div></div>
					<div>
						<small class="text-muted text-center">{!! $data->links() !!}</small>
					</div>
					<small class="text-end">
						<a href="{{ route('persons.create') }}">Adicionar</a>
					</small>
				</div>

			</div>

		</main>

    </body>
</html>
