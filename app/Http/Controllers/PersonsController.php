<?php

namespace App\Http\Controllers;

use App\Models\PersonModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * @referencia
 * 		https://laravel.com/docs/5.3/controllers#resource-controllers
 */
class PersonsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$data = PersonModel::latest()->paginate(10);
		// return view('persons.index',compact('data'))->with('i', (request()->input('page', 1) - 1) * 5);
    return response()->json($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		// return view('persons.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$request->validate([
			'username' => 'required',
			'email' => 'required',
		]);

		PersonModel::create($request->all());
		// return redirect()->route('persons.index')->with('success', 'Usuário adicionado com sucesso.');

    $data = array(
      'status'  => 'success',
      'message' => 'Usuário adicionado com sucesso.'
    );
    return response()->json($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\PersonModel  $personModel
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$person = PersonModel::find($id);
		return view('persons.edit', compact('person'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\PersonModel  $personModel
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$request->validate([
			'username' => 'required',
			'email' => 'required',
		]);

		$person = PersonModel::find($id)->update($request->all());
		// return redirect()->route('persons.index')->with('success', 'Usuário atualizado com sucesso.');

    $data = array(
      'status'  => 'success',
      'message' => 'Usuário atualizado com sucesso.'
    );
    return response()->json($data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\PersonModel  $personModel
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$person = PersonModel::find($id)->delete();
		// return redirect()->route('persons.index')->with('success', 'Usuário deletado com sucesso.');

    $data = array(
      'status'  => 'success',
      'message' => 'Usuário deletado com sucesso.'
    );
    return response()->json($data);
	}

}
