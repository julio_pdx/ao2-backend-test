<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
		@include('_partials.head')
    </head>
    <body>

		<main class="container">

			@include('persons._header')

			@if ($errors->any())
				<div class="alert alert-danger" role="alert">
					<p><strong>Ops!</strong> Algo deu errado...</p>
					<ul>
						@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			<!-- Person List-->
			<div class="my-3 p-3 bg-body rounded shadow-sm">
				<h6 class="border-bottom pb-2 mb-0">Editar</h6>

				@if(empty($person))
					<p>Houve algum erro ao carregar as informações. Tente novamente.</p>
				@else

					<!-- Formulário --> 
					<form action="{{ route('persons.update', $person->id) }}" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						{{ method_field('PUT')}}
						
						<div class="lh-sm border-bottom mb-3">

							<div class="mb-3">
								<label for="username" class="form-label">Usuário</label>
								<div class="input-group mb-3">
									<span class="input-group-text" id="basic-addon1">@</span>
									<input type="text" class="form-control" name="username" id="username" aria-describedby="username-help" value="{{ $person['username'] }}">
								</div>
								<div id="username-help" class="form-text">Nome do usuário.</div>
							</div>

						</div>

						<div class="lh-sm border-bottom mb-3">
							<div class="mb-3">
								<label for="email" class="form-label">E-mail</label>
								<div class="input-group mb-3">
									<input type="email" class="form-control" name="email" id="email" aria-describedby="email-help" value="{{ $person->email }}">
								</div>
								<div id="email-help" class="form-text">E-mail do usuário.</div>
							</div>
						</div>

						<div class="lh-sm border-bottom mb-3">
							<div class="mb-3">
								<label for="website" class="form-label">Website</label>
								<div class="input-group mb-3">
									<input type="text" class="form-control" name="website" id="website" aria-describedby="website-help" value="{{ $person->website }}">
								</div>
								<div id="website-help" class="form-text">Website do usuário.</div>
							</div>
						</div>

						<div class="lh-sm border-bottom mb-3">
							<div class="mb-3">
								<label for="description" class="form-label">Descrição</label>
								<div class="input-group mb-3">
									<textarea class="form-control" name="description" id="description" cols="30" rows="10" aria-describedby="description-help">{{ $person->description }}</textarea>
								</div>
								<div id="description-help" class="form-text">Breve descrição do usuário.</div>
							</div>
						</div>

						<div class="text-end">
							<button type="submit" class="btn btn-sm btn-primary">Salvar Alterações</button>
						</div>
					</form>
				@endif

			</div>
		</main>
	</body>
</html>
