<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    
    protected $table = 'persons';
    protected $fillable = [ 'id', 'username', 'email', 'website', 'description' ];

    public $timestamps = true;
}
