
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Acordo Online</title>

  <link rel="icon" href="https://acordoonline.com/sobre/wp-content/uploads/2017/08/fav32.png" sizes="32x32">
  <link rel="shortcut icon" href="https://acordoonline.com/sobre/wp-content/uploads/2017/08/fav32.png">
  <link rel="apple-touch-icon" href="https://acordoonline.com/sobre/wp-content/uploads/2017/08/fav57.png" sizes="57x57">
  <link rel="apple-touch-icon" href="https://acordoonline.com/sobre/wp-content/uploads/2017/08/fav76.png" sizes="76x76">
  <link rel="apple-touch-icon" href="https://acordoonline.com/sobre/wp-content/uploads/2017/08/fav120.png" sizes="120x120">
  <link rel="apple-touch-icon" href="https://acordoonline.com/sobre/wp-content/uploads/2017/08/fav152.png" sizes="114x114">

	<!-- Fonts -->
	<link href="//fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

	<!-- bootstrap -->
	<link href="//cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" rel="stylesheet" >
	<link href="//cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
	<script src="//cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>


